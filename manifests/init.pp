# @summary
#   This demo module sends a message to the Puppet agent run-time log
#
# @example Basic usage
#   include notify
#
# @param message
#   Specify a string to be written to the Puppet agent log.
#
class notify (
  String $message = 'Just an arbitrary message',
) {
  notify { 'agent log notification':
    message => $message,
  }
}
