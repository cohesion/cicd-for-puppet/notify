require 'spec_helper_acceptance'

describe 'notify' do
  context 'with default parameters' do
    let(:pp) do
      <<-PUPPETCODE
        include notify
      PUPPETCODE
    end

    it 'applies' do
      apply_manifest(pp)
    end
  end

  context 'with a custom parameter' do
    let(:pp) do
      <<-PUPPETCODE
        class { notify:
          message => 'hello litmus',
        }
      PUPPETCODE
    end

    it 'applies' do
      apply_manifest(pp)
    end
  end
end
