# Notify Puppet module with Gitlab CICD pipeline

This repository is an example of how to implement a Gitlab CICD pipeline for a Puppet module, with an emphasis on testing. The Pipeline as Code is defined in [*.gitlab-ci.yml*](https://docs.gitlab.com/ee/ci/yaml/). As an example, this Puppet module sends an arbitrary message to the Puppet agent run-time log.

# The Puppet Development Kit (PDK) 
This module is created with the [*The Puppet Development Kit*](https://github.com/puppetlabs/pdk), which does some boiler plating including Puppet testing tools (https://github.com/puppetlabs/puppet-module-gems/blob/main/config/dependencies.yml).


## Test coverage
When it comes to testing a typical Puppet Roles/Profiles hierarchy, our opinion is that thoroughly testing Puppet modules instead of Puppet Roles provides the most value. 


### Static Analysis
Syntax checking and linting is done by using several testing tools (Rake tasks), for example, https://github.com/voxpupuli/puppet-syntax, https://github.com/voxpupuli/metadata-json-lint, [*Puppet-lint*](https://github.com/rodjek/puppet-lint).
 

### Unit testing
Unit testing Rspec (White-box test) with [*rspec-puppet*](https://github.com/rodjek/rspec-puppet) and [*parallel_tests*](https://github.com/grosser/parallel_tests) for parrallel test execution.


### Acceptance testing
Acceptance testing (Black-box test) with a test harness [*Litmus*](https://github.com/puppetlabs/puppet_litmus).

Because this is a black-box test, we're using a container side-car in the Gitlab pipeline with a [*service*](https://docs.gitlab.com/ee/ci/services/) that runs a Docker in Docker (dind) container that is accessible from our test job container. 


## Usage
The gitlab-ci.yml contains all the test jobs using public container images. By committing a code change to the repository or manually triggering the pipeline will execute the pipeline with its test jobs.
